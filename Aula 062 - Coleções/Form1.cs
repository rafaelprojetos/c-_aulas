﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aula_062___Coleções
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btn_executar_Click(object sender, EventArgs e)
		{
			// Arrays
			string[] nomes = new string[5];
			nomes[0] = "João";
			nomes[1] = "Antonio";
			nomes[2] = "Rafael";
			nomes[3] = "Karol";
			nomes[4] = "Aline";
		}

		private void button1_Click(object sender, EventArgs e)
		{
			// Coleção - LIST
			List<string> NOMES = new List<string>();
			NOMES.Add("João");
			NOMES.Add("Antonio");
			NOMES.Add("Rafael");
			NOMES.Add("Karol");
			NOMES.Add("Aline");
			NOMES.Add("Pedro");
			NOMES.Add("Liza");
			NOMES.Add("Ana");
			NOMES.Add("Joao");

			// Mostra o index da ultima ocorrência de Liza
			MessageBox.Show(NOMES.LastIndexOf("Liza").ToString());

			// Remove o item no index 4
			NOMES.RemoveAt(4);

			// Se tiver a string João na lista
			bool tem_João = NOMES.Contains("João");

			if (tem_João)
			{
				MessageBox.Show("Tem joão");
			}

			// Para cada item dentroda Lista NOMES
			foreach (string item in NOMES)
			{
				lista.Items.Add(item);
			}

			MessageBox.Show(NOMES[0]);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			List<int> inteiros = new List<int>
			{
				0,1,2,3,4,5,6,7,8,9
			};

			foreach (int item in inteiros)
			{
				lista.Items.Add(item);
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			// É como um conjunto Em Python, não tem indice e é mais rápida.

			// Numa HashSet não pode existir valores duplicados
			HashSet<string> Lista_nomes = new HashSet<string>
			{
				"João", "Carlos", "Rui", "Marcos"
			};

			Lista_nomes.Add("Rafael");

			// Se o item não existir será adicionado, caso contrário não
			if (!Lista_nomes.Add("Carlos"))
			{
				MessageBox.Show("Não foi adicionado na HashSet");
			}

			foreach (string item in Lista_nomes)
			{
				lista.Items.Add(item);
			}

			MessageBox.Show(Lista_nomes.Count.ToString());
		}

		private void button5_Click(object sender, EventArgs e)
		{
			Dictionary<string, string> dicionário = new Dictionary<string, string>
			{
				{ "Nome", "Rafael da Silva Sousa" },
				{ "Idade", "18" },
				{ "Sexo", "Masculino" },
				{ "Nascimento", "21/11/2000" },
				{ "Altura", "1.75" },
				{ "Peso", "78" }
			};

			// Pegando as chaves e colocando em uma variável
			string[] lista_com_as_Chaves = dicionário.Keys.ToArray();

			// Pegando os valores e colocando em uma variável
			string[] lista_com_os_Valores = dicionário.Values.ToArray();

			// Loop para usar as 2 listas acima para mostrar a Key e seu Value
			for (int i = 0; i < lista_com_as_Chaves.Count(); i++)
			{
				// Isso é so por curiosidade a forma certa está no proximo loop
				// lista.Items.Add($"{lista_com_as_Chaves[i]}:{lista_com_os_Valores[i]}");
			}

			// Loop que adiociona os dados do dicionário em uma lista.
			foreach (KeyValuePair<string, string> item in dicionário)
			{
				// Adiciona na lista
				// lista.Items.Add(item.Value);

				lista.Items.Add($"{item.Key}: {item.Value}");

			}

			// Mostra o valor agregado ao index Nome
			MessageBox.Show(dicionário["Nome"]);

			// Podemos montar uma estrutura bem mais complexa usando dicionários
			Dictionary<string, List<string>> dados = new Dictionary<string, List<string>>();

			// Adiciona um valor
			dados.Add("Nomes", new List<string> { "Rafael", "Pedro", "Gabriel" });

			// Limpa a lista
			lista.Items.Clear();

			// Adiciona na lista que está linkada a Key Nomes um valor
			dados["Nomes"].Add("Guilherme");

			// Para cada item dentro da lista que está linkada a Key Nomes
			foreach(string item in dados["Nomes"])
			{
				// Mostre na interface.
				lista.Items.Add(item);
			}

			// Podemos verificar se determinado Index(Key) existe
			if (dicionário.ContainsKey("Nome"))
			{
				MessageBox.Show("Sim, contém a chave Nome");
			}
			else
			{
				MessageBox.Show("Não contém a chave Nome");
			}

			// Outro dicionário para exemplificar.
			Dictionary<string, string> dados2 = new Dictionary<string, string>
			{
				{ "Nome", "Rafael" }
			};

			// Podemos verificar se determinado Valor(Value) existe
			if (dados2.ContainsValue("Pedro"))
			{
				MessageBox.Show("Sim, contém o valor Pedro");
			}
			else
			{
				MessageBox.Show("Não contém o valor Pedro");
			}
		}
	}
}
