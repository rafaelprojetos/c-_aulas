﻿namespace Aula_062___Coleções {
	partial class Form1 {
		/// <summary>
		/// Variável de designer necessária.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpar os recursos que estão sendo usados.
		/// </summary>
		/// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código gerado pelo Windows Form Designer

		/// <summary>
		/// Método necessário para suporte ao Designer - não modifique 
		/// o conteúdo deste método com o editor de código.
		/// </summary>
		private void InitializeComponent() {
			this.lista = new System.Windows.Forms.ListBox();
			this.btn_executar = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lista
			// 
			this.lista.FormattingEnabled = true;
			this.lista.Location = new System.Drawing.Point(12, 12);
			this.lista.Name = "lista";
			this.lista.Size = new System.Drawing.Size(443, 420);
			this.lista.TabIndex = 0;
			// 
			// btn_executar
			// 
			this.btn_executar.Location = new System.Drawing.Point(461, 12);
			this.btn_executar.Name = "btn_executar";
			this.btn_executar.Size = new System.Drawing.Size(92, 28);
			this.btn_executar.TabIndex = 1;
			this.btn_executar.Text = "Array";
			this.btn_executar.UseVisualStyleBackColor = true;
			this.btn_executar.Click += new System.EventHandler(this.btn_executar_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(461, 46);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(92, 28);
			this.button1.TabIndex = 2;
			this.button1.Text = "Lista";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(461, 80);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(92, 28);
			this.button2.TabIndex = 3;
			this.button2.Text = "Lista in line";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(461, 114);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(92, 28);
			this.button3.TabIndex = 4;
			this.button3.Text = "Hash Set";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(461, 205);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(92, 28);
			this.button4.TabIndex = 6;
			this.button4.Text = "Hash Set";
			this.button4.UseVisualStyleBackColor = true;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(461, 171);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(92, 28);
			this.button5.TabIndex = 5;
			this.button5.Text = "Dicionário";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(565, 450);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.btn_executar);
			this.Controls.Add(this.lista);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox lista;
		private System.Windows.Forms.Button btn_executar;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
	}
}

