﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Aula_065__066__067___Calculadora
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
            
		}

		private void btn_calcular_Click(object sender, EventArgs e)
		{
            //
			// Faz o calculo da operação
            //
			double parcela1;           // Numeros antes do sinal
			double parcela2;           // Numeros depois do sinal
			double resultado = -1111;  // Resultado final da operação
			string tipo_operacao;      // Sinal passado pelo usuáiro
			string algoritimo = txt_operacao.Text; // Pega o texto que foi passado pelo usuário
			string sinais = "+-*/^";   // Todos os sinais possiveis nessa calculadora
			bool tem_resultado = false;

			// Separa os valores
			int index = 0;
			foreach (char c in algoritimo) // Itera por cada letra da operação
			{
				foreach (char s in sinais) // Itera por cada sinal dentro dos sinais possiveis
				{
					if (s == c)	// Se a letra for um sinal
					{
						// Parcela1
						parcela1 = double.Parse(algoritimo.Substring(0, index));
						
						// Sinal
						tipo_operacao = c.ToString();

						// Parcela2
						parcela2 = double.Parse(algoritimo.Substring(index + 1));

						// analisa a operação que vai ser realizada
						switch (tipo_operacao)
						{
                            // Adição
                            case "+":
								resultado = parcela1 + parcela2;
								break;
                            // Subtração
                            case "-":
								resultado = parcela1 - parcela2;
								break;
                            // Multiplicação
                            case "*":
								resultado = parcela1 * parcela2;
								break;
                            // Divisão
                            case "/":
								resultado = parcela1 / parcela2;
								break;
                            // Divisão
                            case "^":
								resultado = Math.Pow(parcela1, parcela2);
								break;
                            // Caso não seja nenhuma das alternativas
                            default: 
								resultado = 0;
								break;
						}
						break;
					}
				}

				// Analisa o resultado
				if (resultado != -1111)
				{
					tem_resultado = true;
					break;
				}
				else
				{
					tem_resultado = false;
				}

				// acrescenta um index
				index++;
			}

			if (tem_resultado)
			{
				// mostra o resultado
				MessageBox.Show($"{algoritimo} = {resultado}");
			}
			else
			{
				MessageBox.Show("Erro na operação!");
			}
			
			// limpa o algoritmo
			txt_operacao.Text = "";

			// trás o foco para a caixa de texto
			txt_operacao.Focus();
		}

		private void btn_sair_Click(object sender, EventArgs e)
		{
			// Fecha a aplicação
			Application.Exit();
		}

		private void txt_operacao_KeyPress(object sender, KeyPressEventArgs e)
		{
            // Se a pessoa teclar Enter
			if (e.KeyChar == (char)Keys.Enter)
			{
                // Simula o clique no botão calcular
				btn_calcular.PerformClick();
                
                // Sai desse método
				return;
			}

            // Lista de caratecteres permitidos no sistema.
			List<string> caracteres_permitidos = new List<string>()
			{
				"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", " ",
				"+", "-", "/", "*", "/", "^", ","
			};

			//Se não for um caracter permitido 
			if (!caracteres_permitidos.Contains(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
			{
                // então desative a entrada de do teclado
                e.Handled = true;
			}
		}
	}
}