﻿namespace Aula_065__066__067___Calculadora
{
	partial class Form1
	{
		/// <summary>
		/// Variável de designer necessária.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpar os recursos que estão sendo usados.
		/// </summary>
		/// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código gerado pelo Windows Form Designer

		/// <summary>
		/// Método necessário para suporte ao Designer - não modifique 
		/// o conteúdo deste método com o editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.txt_operacao = new System.Windows.Forms.TextBox();
			this.btn_sair = new System.Windows.Forms.Button();
			this.btn_calcular = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txt_operacao
			// 
			this.txt_operacao.Location = new System.Drawing.Point(34, 39);
			this.txt_operacao.MaxLength = 30;
			this.txt_operacao.Name = "txt_operacao";
			this.txt_operacao.Size = new System.Drawing.Size(181, 20);
			this.txt_operacao.TabIndex = 0;
			this.txt_operacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_operacao_KeyPress);
			// 
			// btn_sair
			// 
			this.btn_sair.Location = new System.Drawing.Point(34, 78);
			this.btn_sair.Name = "btn_sair";
			this.btn_sair.Size = new System.Drawing.Size(75, 23);
			this.btn_sair.TabIndex = 1;
			this.btn_sair.Text = "Sair";
			this.btn_sair.UseVisualStyleBackColor = true;
			this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
			// 
			// btn_calcular
			// 
			this.btn_calcular.Location = new System.Drawing.Point(140, 78);
			this.btn_calcular.Name = "btn_calcular";
			this.btn_calcular.Size = new System.Drawing.Size(75, 23);
			this.btn_calcular.TabIndex = 2;
			this.btn_calcular.Text = "Calcular";
			this.btn_calcular.UseVisualStyleBackColor = true;
			this.btn_calcular.Click += new System.EventHandler(this.btn_calcular_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(31, 23);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(91, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Digite a operação";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(248, 153);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btn_calcular);
			this.Controls.Add(this.btn_sair);
			this.Controls.Add(this.txt_operacao);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "Form1";
			this.Text = "Calculadora";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txt_operacao;
		private System.Windows.Forms.Button btn_sair;
		private System.Windows.Forms.Button btn_calcular;
		private System.Windows.Forms.Label label1;
	}
}


