﻿/* Caixa de dialogos
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Aula059
{
    public partial class Form1 : Form
    {
		// Contrutor da classe.
        public Form1()
        {
            InitializeComponent();
        }

		// Quando clicar no messagebox
        private void button1_Click(object sender, EventArgs e)
        {
            // MessageBox

            // informação simples
            // MessageBox.Show("mensagem");

            // informação com titulo
            // MessageBox.Show("mensagem", "titulo");

            // informação com titulo, e botão
            // MessageBox.Show("mensagem", "titulo", MessageBoxButtons.YesNo);

            // informação com titulo, botão e icone
            // MessageBox.Show("mensagem", "titulo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            // Condicional, caso digite sim, caso digite não
            /*
            if (MessageBox.Show("mensagem","titulo",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.No)
            {
                MessageBox.Show("Escolheu a opção NAO");
                return;
            }
            MessageBox.Show("Escolheu a opção SIM");
            */
        }

		// Quando clicar no botão Open FileDialog
        private void button2_Click(object sender, EventArgs e)
        {
			// Instancia uma nova abertura de dialogo
            OpenFileDialog file = new OpenFileDialog();
            
            // titulo da janela
            file.Title = "Escolher ficheiro";

            // diretorio que vai abrir por padrão
            file.InitialDirectory = @"A:\PESSOAIS\Imagens\Wallpapers";

			// permite a seleção de vários arquivos
            file.Multiselect = true;

			// Tudo que vem antes do | é o que será mostrado para o usuário e tudo após é o que fará o filtro dos arquivos
			// file.Filter = "Arquivos TXT(*.txt)|*.txt";

			// Faz 2 filtros diferentes de extensões, JPG e BMP
			file.Filter = "Jpeg Images (*.jpg)|*.jpg|" +
						  "BMP Files (*.bmp)|*.bmp";

			// Lista os arquivos selecionados
			/*
			if (file.ShowDialog() == DialogResult.OK) 
			{
				foreach (var item in file.FileNames) {
					Console.WriteLine(item);
				}
			}
			*/

			// Depois de selecionada a imagem, a mesma é aberta no programa.
			if (file.ShowDialog() == DialogResult.OK) {
				// define o modo como a imagem será apresentada, ou seja o seu dimensionamento
				pic1.SizeMode = PictureBoxSizeMode.Zoom;

				// Endica qual a localização da imagem.
				// pic1.ImageLocation = file.FileName;

				// Define a imagem diretamente
				pic1.Image = Image.FromFile(file.FileName);

				// Para deixa de usar recursos com esse objeto
				file.Dispose();
			}
        }

		// Quando clicar no botão Save FileDialog
		private void button3_Click(object sender, EventArgs e) {

			// Instancia um objeto da classe SaveFileDialog
			SaveFileDialog caixa = new SaveFileDialog();

			// Define o titudo da caixa de diálogo
			caixa.Title = "Gravar ficheiro";

			// Define a localização inicial como sendo a pasta meus documentos
			caixa.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

			// Filtros de salvamentos, word ou txt
			caixa.Filter =
				"Sem extensão(**)|*|" +
				"Arquivo de texto (*.txt)|*.txt|" +
				"Arquivo do word (*.docx)|*.docx";

			// Se clicou em salvar
			if (caixa.ShowDialog() == DialogResult.OK) {
				// cria um arquivo dada sua completa localização da primeira forma.
				// File.Create(caixa.FileName);

				// Cria um arquivo dada sua completa localização da segunda forma.
				StreamWriter arquivo = new StreamWriter(caixa.FileName, false, Encoding.Default);

				// Escreve algum texto no arquivo
				arquivo.WriteLine("Testando se criou o arquivo e gravou algo nele");

				// Libera os recursos usados no arquivo
				arquivo.Dispose();

				// Libera os recursos usados no diálogo
				caixa.Dispose();
			}
		}

		// Quando clicar no botão FolderBrowserDialog
		private void button4_Click(object sender, EventArgs e) {

			// Instancia a biblioteca FolderBrowserDialog para navegar por pastas
			FolderBrowserDialog folder = new FolderBrowserDialog();

			folder.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

			if (folder.ShowDialog() == DialogResult.OK) {
				MessageBox.Show($"A pasta escolhida foi: {folder.SelectedPath}");
			}
			
			folder.Dispose();
		}

		// Quando clicar no botão colorDialog
		private void button5_Click(object sender, EventArgs e) {

			// Instancia o a classe ColorDialog
			ColorDialog color = new ColorDialog();

			// Define qual cor será selecionada na abertura do diálogo, nesse caso a cor da pictureBox
			color.Color = pic1.BackColor;

			// Se a pessoa clicar em OK
			if (color.ShowDialog() == DialogResult.OK) {
				
				// Mostra o nome da cor selecionada
				MessageBox.Show($"A cor selecionada é {color.Color}");

				// Muda a cor de fundo da pictureBox para a cor selecionada no diálogo.
				pic1.BackColor = color.Color;
			}
		}
	}
}
